# App

Flutter App for Blurt


## Key Questions

#### Can we use blurtjs from Dart in flutter apps without re-doing Blurtjs?

IF YES -> Let's build us a very basic flutter app that just displays posts, ASAP, and try to ship it to both app stores, so that we have "a foot in the door".

IF NO -> We have to look into building a dart library that interacts with Blurt.  This would probably force us to address the canonicalization of signatures issue in BlurtJS and Blurt, because I'm not sure how else we might proceed.  

Resources:

- https://github.com/mortea15/fluttersteem
- https://stackoverflow.com/questions/51127653/how-to-use-javascript-library-in-dart#:~:text=You%20need%20to%20add%20a,will%20make%20it%20available%20globally.&text=Dart%20functions%20to%20JavaScript%20functions,forwarded%20to%20the%20JavaScript%20function
- https://api.dart.dev/stable/2.8.4/dart-js/dart-js-library.html
- https://subscription.packtpub.com/book/web_development/9781783989621/5/ch05lvl1sec74/using-javascript-libraries



